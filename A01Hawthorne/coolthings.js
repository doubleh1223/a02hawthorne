function getImage() {
    //get name entered by user
    var fName = document.getElementById("fName").value
    var lName = document.getElementById("lName").value
    const name = fName.toLowerCase()+lName.toLowerCase()

    //get coolness value of name
    var coolness = getCoolness(name.toLowerCase())

    getCoolnessImage(coolness)
    
    var coolTxt = document.getElementById("coolnessLVL")
    coolTxt.innerHTML = coolness
    alert("Your coolness leve is "+coolness+" so your image is.....")

}

function getCoolness(name) {
    var coolness = 0

    if (name === undefined) {
        return 0
    }
    if (!isNaN(name)) {
        return 0
    }
    //iterate through name and get coolness value for each letter
    for (var i = 0; i < name.length; i++) {
        
        
        if (!(/^[a-z ]*$/.test(name.charAt(i)))) {
            coolness -= 10
        }else {
            coolness += getCoolnessByLetter(name.charAt(i))
        }
    }

    //add/subtract coolness based on special characteristics
    if (name.charAt(name.length - 1) === "z") {
        coolness += 10
    }

    
    
    if (name === "dillon" || name === "dillondow" || name === "dillon ashley dow") {
        return 100000000
    }


    var netflix = document.getElementById("netflix")
    var mattdamon = document.getElementById("mattdamon")
    var eating = document.getElementById("eating_healthy")
    var smoking = document.getElementById("smoking")

    if (netflix != null || mattdamon != null || eating != null || smoking != null) {
    if (netflix.checked === true) {
        coolness += 5
    }
    if (mattdamon.checked === true) {
        coolness += 2
    }
    if (eating.checked === true) {
        coolness += 3
    }
    if (smoking.checked === true) {
        coolness -= 4
    }
}

    return coolness
}

function getCoolnessByLetter(letter) {

    //switch statement that returns the coolness value by letter
    switch (letter) {
        case 'a':
            return -3;
            break;

        case 'b':
            return 7;
            break;

        case 'c':
            return 4;
            break;

        case 'd':
            return 5;
            break;

        case 'e':
            return 2;
            break;

        case 'f':
            return 7;
            break;
            
        case 'g':
            return 9;
            break;

        case 'h':
            return 6;
            break;

        case 'i':
            return -1;
            break;

        case 'j':
            return 4;
            break;

        case 'k':
            return 4;
            break;

        case 'l':
            return 4;
            break;

        case 'm':
            return -4;
            break;

        case 'n':
            return -5;
            break;
            
        case 'o':
            return 1;
            break;

        case 'p':
            return 5;
            break;

        case 'q':
            return 8;
            break;

        case 'r':
            return 5;
            break;

        case 's':
            return 4;
            break;

        case 't':
            return -5;
            break;

        case 'u':
            return 4;
            break;

        case 'v':
            return 7;
            break;
            
        case 'w':
            return 6;
            break;

        case 'x':
            return 10;
            break;
        
        case 'y':
            return 5;
            break;

        case 'z':
            return 10;
            break;

        default:
            return 0;
            break;
    }
}

function getCoolnessImage(coolness) {
    //const dir = "'Cool_Picutres\'";

    var img = document.getElementById("cool_picture");
    if (coolness > 0) {
        if (coolness > 37 && coolness != 100000000) {
            img.src = "Cool_Pictures/cool/max.jpg"            
        }else {
            img.src = "Cool_Pictures/cool/"+coolness+".jpg"
        }
    }else if (coolness < 0) {
        if (coolness > -26) {
            img.src = "Cool_Pictures/not_cool/"+coolness+".jpg"
        }else{
            img.src = "Cool_Pictures/not_cool/max.jpg"
        }
    }else if (coolness === 0) {
        img.src = "Cool_Pictures/0.jpg"
    }

}
