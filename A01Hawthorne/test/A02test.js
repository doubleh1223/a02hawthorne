


QUnit.test('Testing our max function with four sets of inputs', function (assert) {
    assert.strictEqual(getCoolness(), 0, 'No parameters');
    assert.strictEqual(getCoolness('hello'), 17, 'hello as name');
    assert.strictEqual(getCoolness('!@#$%!@#$%'), -100, 'random symbols');
    assert.strictEqual(getCoolness(1234), 0, 'entering numbers');
});







